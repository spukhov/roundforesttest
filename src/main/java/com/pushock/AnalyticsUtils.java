package com.pushock;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AnalyticsUtils {
    public static final String MOST_ACTIVE_USERS_QUERY = "SELECT UserId, count(*) as c FROM review GROUP BY UserId SORT BY c DESC LIMIT 1000";
    public static final String MOST_COMMENTED_ITEMS_QUERY = "SELECT ProductId, count(*) as c FROM review GROUP BY ProductId SORT BY c DESC LIMIT 1000";
    public static final String SELECT_TEXT_FROM_REVIEW_QUERY = "SELECT Text FROM review";

    public static List<String> get1000MostActiveUsers(SparkSession spark) {
        Dataset<Row> sqlResult = spark.sql(MOST_ACTIVE_USERS_QUERY);
        return sqlResult.javaRDD()
                .map(row -> row.getString(0))
                .collect()
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> get1000MostCommentedFoodItems(SparkSession spark) {
        Dataset<Row> sqlResult = spark.sql(MOST_COMMENTED_ITEMS_QUERY);
        return sqlResult.javaRDD()
                .map(row -> row.getString(0))
                .collect()
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> get1000MostUsedWords(SparkSession spark) {
        Dataset<Row> sqlResult = spark.sql(SELECT_TEXT_FROM_REVIEW_QUERY);
        return sqlResult.javaRDD().flatMap((FlatMapFunction<Row, String>) row -> Arrays.asList(row.getString(0)
                .toLowerCase()
                .split("[^\\w']+"))
                .iterator())
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) -> a + b)
                .mapToPair(a -> new Tuple2<>(a._2(), a._1()))
                .sortByKey(false)
                .take(1000)
                .stream()
                .map(Tuple2::_2)
                .sorted()
                .collect(Collectors.toList());
    }
}
