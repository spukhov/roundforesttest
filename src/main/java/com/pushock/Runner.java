package com.pushock;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import java.util.List;

/**
 * Hi, there!
 * The task was implemented using Apache Spark
 * Here is the list of things that were not made in time:
 * - Item number 4
 * - Tests
 * -
 *
 * */
public class Runner {

    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", System.getProperty("user.dir"));
        String reviewsFilePath = "src/main/resources/reviews.csv";

        SparkSession spark = prepareSparkSession(reviewsFilePath);

        List<String> mostActiveUsers = AnalyticsUtils.get1000MostActiveUsers(spark);
        List<String> mostCommentedFoodItems = AnalyticsUtils.get1000MostCommentedFoodItems(spark);
        List<String> mostUsedWords = AnalyticsUtils.get1000MostUsedWords(spark);

        printResults(mostActiveUsers, "1. 1000 most active users: ");
        printResults(mostCommentedFoodItems, "2. 1000 most commented items: ");
        printResults(mostUsedWords, "3. 1000 most commented items: ");
    }

    private static void printResults(List<String> results, String message) {
        System.out.println("\n" + message + "\n");
        results.forEach(System.out::println);
    }

    private static SparkSession prepareSparkSession(String reviewsFilePath) {

        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL Example")
                .master("local")
                .getOrCreate();

        Logger.getRootLogger().setLevel(Level.ERROR);

        StructType schema = new StructType()
                .add("Id", "long")
                .add("ProductId", "string")
                .add("UserId", "string")
                .add("ProfileName", "string")
                .add("HelpfulnessNumerator", "string")
                .add("HelpfulnessDenominator", "string")
                .add("Score", "string")
                .add("Time", "string")
                .add("Summary", "string")
                .add("Text", "string");

        Dataset<Row> dataset = spark.read()
                .schema(schema)
                .csv(reviewsFilePath);

        dataset.printSchema();

        dataset.createOrReplaceTempView("review");

        return spark;
    }
}
